package fr.ensg.yboudili.maven.plugins.fibonacci;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import fr.ensg.yboudili.fibonacci.core.Fibonacci;

/**
 * FibonacciMojo is the class that represents the fibonacci goal of the plugin
 * @author Yassmine BOUDILI yasmine.boudili@ensg.eu
 */
@Mojo(name="fibonacci")
public class FibonacciMojo extends AbstractMojo{

	/**
	 * rank is the parameter that represents the rank of the calculated Fibonacci number in the Fibonacci sequence
	 */
	@Parameter(property = "rank")
    private int rank;

	/**
	 * execute is the method that executes the goal and prints the calculated Fibonacci number
	 */
	public void execute(){
			System.out.println(Fibonacci.calculate(rank));
	}
}
