
# Fibonacci : projet maven multi-modules avec intégration continue

## Description
Ce document constitue le rapport des deux projets Maven et Intégration Continue de la filière TSI 2018/2019, École Nationale des Sciences Géographiques.

## Partie 1 : projet maven

### Description
Le projet Fibonacci comporte une application web Spring-Boot ainsi qu'un plugin qui renvoient le nombre Fibonacci d'un rang donné.  
Le projet est organisé en 3 modules:  
* **core**: Le coeur du projet contenant la classe de calcul de la série de Fibonacci
* **plugin**: Le plugin maven
* **webapp**: L'application web Spring-Boot

### Installation
 1. Cloner le dépôt  
 ``git clone https://gitlab.com/Yassmine.Boudili/fibonacci ``  
 ``cd fibonacci``
 2. Installer les binaires  
 ``mvn install``

### Tester le plugin maven
Pour tester le plugin maven et obtenir le nombre Fibonacci du rang n, tapez les commandes suivantes :  
``mvn fr.ensg.yboudili.fibonacci:fibonacci-maven-plugin:1.0.0-SNAPSHOT:fibonacci -Drank=n``

### Tester l'application web
Pour tester l'application web et obtenir le nombre Fibonacci du rang n, tapez les commandes suivantes :
1. ``mvn package``
2. ``cd webapp/target/``
3. ``java -jar fibonacci-webapp-1.0.0-SNAPSHOT.jar``
4. Ensuite dans tapez l'adresse suivante dans la barre de recherche de votre navigateur  
``http://localhost:8080/fibonacci/n``

### Documentation du projet
Pour générer et visualiser la documentation du projet, tapez la commande suivante : ``mvn site site:stage``  
Ensuite, accédez aux pages de documentation se trouvant dans le dossier "*target/staging/*" .  

Vous pouvez également consulter la documentation déployée en ligne sur les pages Gitlab à travers les liens suivants:  
Projet maven: https://yassmine.boudili.gitlab.io/fibonacci/project-info.html  
JavaDocs: https://yassmine.boudili.gitlab.io/fibonacci/apidocs/index.html

## Partie 2 : intégration continue

### Introduction
La partie Intégration Continue du projet a été réalisé grâce à l'outil CI/CD de Gitlab en se basant sur un fichier template .gitlab-ci.yml pour Maven.

### Description du pipeline
Le pipeline mis en place se compose des étapes suivantes:

1. **Branche dev**  
![-- dev_pipeline --](screenshots/dev_pipeline.png)
* Étape **build** sur jdk8 qui construit le projet et génère la documentation (``mvn test-compile site``) ;
* Étape **test** en parallèle sur jdk8 et jdk11 qui exécute les tests unitaires pour vérifier le projet (``mvn verify``)  .


2. **Branche master**  
![-- master_pipeline --](screenshots/master_pipeline.png)
* Étape **build** sur jdk8 qui construit le projet et génère la documentation (``mvn test-compile site``) ;
* Étape **test** sur jdk8 qui génère et archive la documentation staging du projet (``mvn site site:stage``) ;
* Étape **deploy** qui déploie la documentation archivée sur Gitlab pages .
  
 ### Merge requests
Une merge request de la branche dev vers master déclenche le pipeline sur la branche dev et merge les deux branches en cas de succés du pipeline. Ci-dessous deux éxemples de merge requests: la première pour le cas d'échec en cassant un test unitaire, et la deuxième pour le cas de succés.  
  
1. ![-- failed_merge --](screenshots/failed_merge.png)   

2. ![-- passed_merge --](screenshots/passed_merge.png)

## Difficultés pour les deux projets
Dans sa globalité, le projet maven n'a pas présenté beaucoup de contraintes lors de sa réalisation, notamment pour les premières parties du moment qu'il se rapproche du TP réalisé en classe.   

Néanmoins, la partie de documentation a été un peu contraignante pour les raisons suivantes:
* Difficulté à retrouver les bons plugings à ajouter pour générer un site fonctionnel;
* Problème de version pour le plugin maven site lors du passage à la partie intégration continue. (version 3.7.1 qui fonctionnait localement mais pas sur les images maven lors de l'éxécution du pipeline)

Finalement, j'ai réussi à résoudre ces problèmes et générer la documention (maven et JavaDocs) et la déployer sur les pages Gitlab. Toutefois, en ce qui concerne la partie publication j'ai essayé un peu de suivre cet article: https://docs.gitlab.com/ee/ci/examples/artifactory_and_gitlab/, mais je n'ai pas réussi à déloyer le projet.

---
Réalisé par : Yassmine BOUDILI
